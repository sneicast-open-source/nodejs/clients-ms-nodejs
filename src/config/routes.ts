import { Router } from 'express';
import { ClientRoutes } from './../app/clients/client.routes';



export class Routes {
    private routesList: Array<Router> = new Array();

    /*inicializar routes*/
   
    private clientRoutes: ClientRoutes = new ClientRoutes();
    
    public getAllRoutes(): Router[] {
        
        this.routesList.push(this.clientRoutes.getRouter());
        

        return this.routesList;
    }
}

