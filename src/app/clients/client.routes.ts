import { Router } from 'express';
import { ClientController } from './client.controller';

export class ClientRoutes {
    private router = Router();
    private clientController: ClientController = new ClientController();
   
    public getRouter(): Router {
        this.router.route('/clients')
            .get( this.clientController.getAllClients);

        return this.router;

    }

}