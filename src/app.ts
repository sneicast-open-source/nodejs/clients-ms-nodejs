import express from 'express';
import cors from 'cors';

import { Routes} from "./config/routes";

class App {
    public app: express.Application = express();
    private routes: Routes = new Routes();
    
    constructor() {
        this.settings();
        this.middlewares();
        this.routesSetup();
    }

    private settings(): void {
        this.app.set("port", process.env.PORT || 8080);
        if (process.env.NODE_ENV !== "production") {
          require("dotenv").config();
        }
    }

    private middlewares(): void {
        // middlewares
        this.app.use(cors({ origin: '*' }))

        this.app.use(function (req, res, next) {
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization");
          res.header("Access-Control-Allow-Methods", "POST, GET, PUT, PATCH, DELETE");
          next();
        });
        //this.app.use(bodyParser.urlencoded({ extended: false }));
        //this.app.use(bodyParser.json({ limit: '4mb' }));
      }

      private routesSetup(): void {
        this.routes.getAllRoutes().forEach(router => {
          this.app.use("/api", router);
        });
      }

      
}

export default new App().app;