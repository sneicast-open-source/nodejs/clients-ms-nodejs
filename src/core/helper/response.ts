import { Response } from 'express';
//import { CustomError } from '../../app/common/common.model';

//Leer: https://developer.mozilla.org/es/docs/Web/HTTP/Status

/*Respuestas satisfactorias*/
export function ok(res: Response, message: Object) {
    return res.status(200).json(message);
}
export function created(res: Response, message: Object) {
    return res.status(201).json(message);
}
export function accepted(res: Response, message: Object) {
    return res.status(202).json(message);
}
export function noContent(res: Response, message: Object) {
    return res.status(204).json(message);
}


/*Errores de cliente*/
export function badRequest(res: Response, code: string, message: Object, errors: object) {
    return res.status(400).json({ code, message, errors });
}
export function unauthorized(res: Response, message: Object) {
    return res.status(401).json(message);
}
export function forbidden(res: Response, message: Object) {
    return res.status(403).json(message);
}
export function notFound(res: Response, message: Object): Response {
    return res.status(404).json(message);
}
export function conflict(res: Response, message: Object) {
    return res.status(409).json(message);
}

export function unprocessableEntity(res: Response, message: Object) {
    return res.status(422).json(message);
}
export function upgradeRequired(res: Response, message: Object) {
    return res.status(422).json(message);
}



/*Errores de servidor*/
export function internalServerError(res: Response, message: Object) {
    return res.status(500).json(message);
}
export function serviceUnavailable(res: Response, message: Object) {
    return res.status(503).json(message);
}
export function gatewayTimeout(res: Response, message: Object) {
    return res.status(504).json(message);
}

/*CustomResponse*/
/*export function customResponse(res: Response, message: CustomError): Response {
    if(message.code === '404'){
        return notFound(res, message);
    } else if(message.code === '401'){
        return unauthorized(res, message);
    }
    return internalServerError(res, message);
}
*/

